
#=====================================================================================#
#
# Script de conversion des fichires .txt et .csv en .feather
#
#=====================================================================================#



library(feather)
library(magrittr)
library(dplyr)

#Import des tables ----

# table de correspondance CIM-10
CIM10_table <- read.delim(
  file = paste0(getwd(), "/tables_libelles/LIBCIM10_re-encoded.txt"),
  header = TRUE,
  sep = "|",
  colClasses = "character",
  fileEncoding = "utf-8"
)


# table de correspondance CCAM
CCAM_table <- read.delim(
  file = paste0(getwd(),"/tables_libelles/CCAM_ACTES_re-encoded.txt"),
  header = TRUE,
  sep = "|",
  colClasses = "character",
  fileEncoding = "utf-8"
)


# table de correspondance GHM
GHM_table <- read.delim(
  file = paste0(getwd(), "/tables_libelles/LIBGHMFG_re-encoded.txt"),
  header = TRUE,
  sep = "|",
  colClasses = "character",
  fileEncoding = "utf-8"
)

# table de correspondance codes erreurs
error_table <- read.delim(
  file = paste0(getwd(),"/tables_libelles/LIBERRFG_re-encoded.txt"),
  header = TRUE,
  sep = "|",
  colClasses = "character",
  fileEncoding = "utf-8"
)

# table des autoridations UM
ium_table <- read.csv2(
  file = paste0(getwd(), "/tables_libelles/NomenUM_UF.csv"), 
  header = F, col.names = c("code_um", "lib", "mode_hosp"), as.is = T
) %>%
  mutate(code_um = unlist(lapply(code_um, function(i){if (nchar(i) <2) paste0("0", i) else i}))) %>%
  mutate(lib = paste0(code_um, " - ", lib))


# table des niveaux de severite des CMA
CMA_table <- read.csv(file = paste0(getwd(), "/listes_CMA/Liste_CMA_2018.csv"))

# table des exlusions de CMA
exclusions_table <- read.csv2(file = paste0(getwd(), "/listes_CMA/Liste_exclusion_FG_v2018.csv"))


# Export au format feather ----

write_feather(CIM10_table, "./tables_libelles/CIM10.feather")
write_feather(CCAM_table, "./tables_libelles/CCAM.feather")
write_feather(GHM_table, "./tables_libelles/GHM.feather")
write_feather(error_table, "./tables_libelles/errors.feather")
write_feather(ium_table, "./tables_libelles/ium.feather")
write_feather(CMA_table, "./listes_CMA/CMA.feather")
write_feather(exclusions_table, "./listes_CMA/exclusions.feather")

