#
# 
# Liste de fonctions support tirées du "regroupeur RSS" réalisé par Stéphane Said
# Permettent de récupérer les résultats de la fonction groupage à partir des fichier txt générés en sortie
#   et de calculer la valorisation des séjours
# 



#' Récupération des fichiers de sortie de la fonction groupage
#'
#' @param output_path chemin d'accès au dossier de sortie de la FG
#' @param input_file nom du fichier .txt d'input du RSS
#' @param dico_file nom du fichier dico comprenant la position des champs dans le fichier RDS de sortie
#' @param annee année de la fonction groupage
#' 
getResultsFG <- function(
  output_path = paste0(getwd(), "/output"),
  input_file = "inputRSS.txt",
  dico_file = paste0(getwd(), "/input/dicords.csv"),
  annee = 2018) {

  
  # Recuperation des output : ----
  output_files <- paste0(output_path,"/",input_file)
  
  # Fichier de log : 
  # (non indispensable)
  # log_exec <- readr::read_delim(file = paste0(output_files, ".log"),col_names = c("INDICATEUR", "VALEUR"), delim = ":",
  #                               locale = locale(encoding = "latin9"), col_types = "cc") %>%
  #   mutate_all(str_trim) %>%
  #   mutate(VALEUR = as.integer(VALEUR))
  
  
  # Fichier d'erreurs
  err_exec <- readr::read_lines(file = paste0(output_files,".err")) %>% 
    as_data_frame()
  names(err_exec) <- "X1"
  
  err_exec <- err_exec %>% 
    tidyr::separate("X1",
                    into = c("NUMRSS","NUMUM","CMD","GHM","CODE_RETOUR","ERREURS"), 
                    sep = ",", extra = "merge") %>% 
    mutate_all(str_trim) %>% 
    mutate(GHM = str_c(CMD,GHM)) %>% 
    select(-CMD)
  
  #Fichier supplementaire
  sup_exec <- readr::read_lines(file = paste0(output_files,".sup")) %>% 
    as_data_frame()
  names(sup_exec) <- "X1"
  sup_exec <- sup_exec %>% 
    tidyr::separate("X1",
                    into = c("NUMRSS","NUMUM","CMD","GHM","SEJ_PRINC","NB_RUM","NB_ERR","ERR_NON_BLOQUANTES"), 
                    sep = ",", extra = "merge") %>% 
    mutate_all(str_trim) %>% 
    mutate_at(c("NB_RUM", "NB_ERR"), as.integer) %>% 
    mutate(GHM = str_c(CMD,GHM)) %>% 
    select(-CMD)
  
  # Fichier de valorisation rds
  rds_exec <- readRDS(rds_file = paste0(output_files,".rds"), dico_file = dico_file, annee = annee)
  
  # Formattage de l'output ----
  output <- list(#log = log_exec, 
                 err = err_exec,
                 sup = sup_exec,
                 rds = rds_exec)
  
  return(output)
}
 

#' Lire un fichier RDS MCO
#'
#' @param rds_file 
#' @param dico_file 
#' @param annee 
#'
#' @return
#' @export
#'
#' @examples
readRDS <- function(rds_file, dico_file, annee){
  
  dico <- read.csv2(dico_file, stringsAsFactors = F)
  donnees <- readr::read_delim(
    rds_file, delim = "\n", col_names = FALSE, col_types = "c", trim_ws = FALSE) %>%
  mutate(VERSION = str_sub(X1, 30, 32)) %>%
  split(.$VERSION)
  
  
  
  traitement <- 
    purrr::map_df(donnees, ~{
      subdico <- dico %>% 
        filter(ANNEE == annee, VERSION == .x$VERSION[1], TABLE_BDD == "MCO_RDS") %>% 
        arrange(DEB)

      colonnes_fixes <- subdico %>%select(NOM_CHAMP) %>% .[[1]]
      section_fixe <- subdico %>% select(FIN) %>% .[[1]]
      fixe_int <- subdico %>% filter(FORMAT_R == "integer") %>% select(NOM_CHAMP) %>% .[[1]]
      
      output <- .x %>%
        tidyr::separate_(col = "X1", into = c(colonnes_fixes, "colonnes_variables"),
                         sep = section_fixe, remove = T) %>%
        # select(-colonnes_variables) %>% 
        mutate_all(str_trim) %>% 
        mutate_at(fixe_int, as.integer)

      return(output)
    })

  return(traitement)
}


#' Calculer la valorisation
#'
#' @param rds_data 
#' @param tarifs 
#' @param supplements 
#'
#' @return
#' @export
#'
#' @examples
calculerValo <- function(rds_data, tarifs, supplements){
  
  rds <- rds_data %>% 
    mutate(FG_VERSION = ifelse(as.integer(RDS_MOISSOR) < 3,
                               as.integer(RDS_ANNEESOR) - 1,
                               as.integer(RDS_ANNEESOR))) %>% 
    left_join(tarifs, by = c("RDS_GHS" = "NUMGHS", "FG_VERSION")) %>% 
    mutate_at(vars(SEUIL_BAS:TARIF_EXH), funs(ifelse(is.na(.), 0, .))) %>% 
    left_join(supplements, by = "FG_VERSION")
  
  valo <- data_frame(
    NUMRSS = rds$RDS_NUMRSS,
    GHM = rds$RDS_GHM,
    GHS = rds$RDS_GHS,
    TARIF_GHS = rds$TARIF_GHS,
    SEUIL_BAS = rds$SEUIL_BAS, # add_on
    BORNE_BASSE = rds$RDS_NBEXB,
    TARIF_BORNE_BASSE = -(rds$RDS_NBEXB * rds$TARIF_EXB + ifelse(rds$RDS_NBEXB > 0,1,0) * rds$FORFAIT_EXB),
    SEUIL_HAUT = rds$SEUIL_HAUT, # add-on
    BORNE_HAUTE = rds$RDS_NBEXH,
    TARIF_BORNE_HAUTE = rds$RDS_NBEXH * rds$TARIF_EXH,
    NB_REP = rds$RDS_NBREP,
    FORFAIT_REP = rds$RDS_NBREP * rds$REP,
    NB_REA = rds$RDS_NBREA,
    FORFAIT_REA = rds$RDS_NBREA * rds$REA,
    NB_STF = rds$RDS_NBSTF,
    FORFAIT_STF = rds$RDS_NBSTF * rds$STF,
    NB_STFREA = rds$RDS_NBSTFREA,
    FORFAIT_STFREA = rds$RDS_NBSTFREA * rds$STF, ###
    NB_SRC = rds$RDS_NBSRC,
    FORFAIT_SRC = rds$RDS_NBSRC * rds$SRC,
    NB_NN1 = rds$RDS_NBNN1,
    FORFAIT_NN1 = rds$RDS_NBNN1 * rds$NN1,
    NB_NN2 = rds$RDS_NBNN2,
    FORFAIT_NN2 = rds$RDS_NBNN2 * rds$NN2,
    NB_NN3 = rds$RDS_NBNN3,
    FORFAIT_NN3 = rds$RDS_NBNN3 * rds$NN3,
    NB_ANT = rds$RDS_NBANTE,
    FORFAIT_ANT = rds$RDS_NBANTE * rds$ANT,
    NB_SDC = as.numeric(ifelse("RDS_SDC" %in% colnames(rds),
                               ifelse(is.na(rds$RDS_SDC), 0, rds$RDS_SDC),
                               0)),
    FORFAIT_SDC = NB_SDC * rds$SDC 
  ) %>% 
    mutate(FORFAIT_STF = FORFAIT_STF - FORFAIT_STFREA)
  
  return(valo)
  
}



#' Appeler la fonction groupage
#'
#' @param exe_fg chemin de l'executable de la fonction groupage
#' @param rum_file chemin du fichier rss
#' @param path_tables repertoire des tables de la fonction groupage
#' @param statut 1 = ex-DGF, 2 = ex-OQN
#' @param ium_file chemin du fichier des unites medicales
#' @param path_sortie repertoire de sortie de l'execution de la fg
#'
#' @return
#' @export
#'
#' @examples
callFg <- function(exe_fg, rum_file, path_tables, statut, ium_file, path_sortie, 
                   bat_name = tempfile(fileext = ".bat"), keep_bat = FALSE){
  
  # Controles de coherence
  path_tables <- gsub("/$|\\\\$", "", path_tables)
  path_sortie <- gsub("/$|\\\\$", "", path_sortie)
  
  if(! as.character(statut) %in% c("1","2")){
    stop("Le parametre statut prend pour valeur '1' (ex-DGF) ou '2' (ex-OQN)")
  }
  
  # preparation du bat
  exe <- paste0('"',normalizePath(exe_fg),'"')
  entree <- paste0('"',normalizePath(rum_file),'"')
  tables <- paste0('"', normalizePath(path_tables), '"\\')
  ium <- paste0('"', normalizePath(ium_file),'"')
  sortie <-  paste0('"', normalizePath(path_sortie), '"\\')
  
  # Enregistrement du bat
  bat <- paste(exe, entree, tables, statut, ium, sortie)
  sink(file = bat_name)
  cat(bat)
  sink()
  
  # Execution du bat
  exec <- system(bat_name)
  
  if(exec != 0){
    warning(paste0("L'execution de la Fg renvoie le code de sortie ", exec))
  }
  
  if(!keep_bat){
    file.remove(bat_name)
  }
  
  return(exec)
}


#' Calculer l'impact tarifaire d'un changement de niveau de sévérité du GHM
#'
#' @param tarifs la data.frame des tarifs renvoyé par l'exécution du scripts tarifs
#' @param num_GHM le numéro de GHM sur 5 caractères
#' @param num_GHS le numéro de GHS sur 4 caractères (en commençant par 0 si besoin)
#' @param duree_sejour nombre de nuits
#' @param fg_version annee de version FG : 2016, 2017, 2018, etc.
#'
#' @return
#' @export
#'
#' @examples
impact_chgt_niveau <- function(tarifs, num_GHM, num_GHS, duree_sejour, fg_version = 2018){
  tarifs <- tarifs %>% filter(FG_VERSION == fg_version)
  
  niv <- as.integer(substr(tarifs[tarifs$NUMGHS == num_GHS,]$GHM, 6, 6))
  if (!(niv %in% c(1, 2, 3, 4))) return(list(impact = F))
  
  index <- which(tarifs$NUMGHS == num_GHS & tarifs$GHM == num_GHM)
  row_indices <- c()
  if (niv == 1) {
    row_indices[1] <- index
    row_indices[2] <- index + 1
    row_indices[3] <- index + 2
    row_indices[4] <- index + 3
  } else if (niv == 2) {
    row_indices[1] <- index - 1
    row_indices[2] <- index
    row_indices[3] <- index + 1
    row_indices[4] <- index + 2
  } else if (niv == 3) {
    row_indices[1] <- index - 2
    row_indices[2] <- index - 1
    row_indices[3] <- index
    row_indices[4] <- index + 1
  } else if (niv == 4) {
    row_indices[1] <- index - 3
    row_indices[2] <- index - 2
    row_indices[3] <- index - 1
    row_indices[4] <- index
  }
  
  GHS <- tarifs[row_indices,]$NUMGHS
  GHM <- tarifs[row_indices,]$GHM
  SEUIL_BAS <- tarifs[row_indices,]$SEUIL_BAS
  SEUIL_HAUT <- tarifs[row_indices,]$SEUIL_HAUT
  TARIF_GHS <- tarifs[row_indices,]$TARIF_GHS
  TARIF_B_BASSE <- tarifs[row_indices,]$TARIF_EXB
  TARIF_B_HAUTE <- tarifs[row_indices,]$TARIF_EXH
  DELTA <- unlist(lapply(1:4, function(i){
    TARIF_GHS[i] - TARIF_GHS[niv] + 
      ifelse(duree_sejour - SEUIL_BAS[i] < 0, (duree_sejour - SEUIL_BAS[i]) * TARIF_B_BASSE[i], 0) - 
      (ifelse(duree_sejour - SEUIL_BAS[niv] < 0, (duree_sejour - SEUIL_BAS[niv]) * TARIF_B_BASSE[niv], 0)) + 
      ifelse(duree_sejour - SEUIL_HAUT[i] > 0, (duree_sejour - SEUIL_HAUT[i]) * TARIF_B_HAUTE[i], 0) - 
      (ifelse(duree_sejour - SEUIL_HAUT[niv] > 0, (duree_sejour - SEUIL_HAUT[niv]) * TARIF_B_HAUTE[niv], 0))  
    
  }))
  INFO <- unlist(lapply(1:4, function(i){
    if(i > 1 && duree_sejour < i + 1) "<font color=\"red\"><b><em>Durée insuffisante</em></b></font>"
    else if (DELTA[i] == 0) "<b>-</b>"
    else paste0(
      if (DELTA[i] > 0) "<font color=\"green\"><b>+"
      else "<font color=\"red\"><b>",
      round(DELTA[i], digits = 0), " \u20AC", "</b></font>"
    )
  }))
  
    
  return(list(impact = T, tarifs = data.frame(GHS, GHM, SEUIL_BAS, SEUIL_HAUT, TARIF_GHS, TARIF_B_BASSE, TARIF_B_HAUTE, DELTA, INFO)))
  
}


#' Generation d'un bloc html pour affichage d'un tableau
#'
#' @param df data.frame source
#'
#' @return
#' @export
#'
#' @examples
table_html <- function(df){
  s <- paste0("<table class=\"table-condensed\">","<tr>")
  for (i in 1:ncol(df)) s <- paste0(s, "<th>", colnames(df)[i], "</th>")
  s <- paste0(s, "</tr>")
  s <- paste0(s, "<tr><td style=\"border-top:1px solid lightgray;\" colspan = \"", ncol(df),"\"></td></tr>")
  for (i in 1:nrow(df)) {
    s <- paste0(s, "<tr>")
    for(j in 1:ncol(df)) s <- paste0(
      s, "<td>", df[i, j], "</td>"
    )
    s <- paste0(s, "</tr>")
  }
  s <- paste0(s, "</table>")
  return(s)
}


#' Génération du code html pour restitution d'un rapport de résultats exportable
#'
#' @param UI_id string identifiant le module de sortie des résultats
#' @param panel UI panel de sortie de la fonction groupage
#' @param success boolean indiquant si le groupage a résussi ou si une erreur bloquante a été retournée
#' @param df_errors data.frame des erreurs (bloquantes ou non selon les circonstances)
#' @param df_DAS data.frame des DAS
#' @param df_forfaits data.frame des suppléments
#'
#' @return
#' @export
#'
#' @examples
rapport_html <- function(UI_id, panel, success, df_errors = data.frame(), df_DAS = data.frame(), df_forfaits = data.frame()){
  
  # extraction du code html du UI panel de sortie du groupage (la fonction renderTags n'extrait pas les résultats des renderTable...)
  s <- htmltools::renderTags(panel)$html
  
  # test if groupage failed because of erreur bloquante
  #   Rq : en pratique l'export n'est pas possible (car non pertinent) en cas d'erreur bloquante
  #   Cependant on laisse ici la structure générale en cas d'évolution future
  if (!success){
    s <- unlist(strsplit(s, split = paste0("<div id=\"", UI_id, "-liste_codes_erreurs\" class=\"shiny-html-output\"></div>"), fixed = T))
    return(paste0(s[1], table_html(df = df_errors), s[2]))
  }
  
  # else (function has not returned yet)
  #   incorporation html des data.frames non vides
  if (!is_empty(df_DAS)) {
    s <- unlist(strsplit(s, split = paste0("<div id=\"", UI_id, "-liste_DAS\" class=\"shiny-html-output\"></div>"), fixed = T))
    s <- paste0(s[1], table_html(df = df_DAS), s[2])
  }
  if (!is_empty(df_forfaits)) {
    s <- unlist(strsplit(s, split = paste0("<div id=\"", UI_id, "-liste_forfaits\" class=\"shiny-html-output\"></div>"), fixed = T))
    s <- paste0(s[1], table_html(df = df_forfaits), s[2])
  }
  if (!is_empty(df_errors)) {
    s <- unlist(strsplit(s, split = paste0("<div id=\"", UI_id, "-liste_codes_erreurs\" class=\"shiny-html-output\"></div>"), fixed = T))
    s <- paste0(s[1], table_html(df = df_errors), s[2])
  }
  
  return(s)
  
}



#' Récupère des infos complémentaires depuis un fichier RDS MCO déjà lu avec readRDS
#'
#' @param rds tibble rds restitué par la fonction readRDS
#' @param dico_file dico au format dicords.csv comportant les libelles des champs
#'
#' @return
#' @export
#'
#' @examples
getMoreInfoFromRDS <- function(rds, dico_file){
  dico <- read.csv2(dico_file, stringsAsFactors = F) %>% 
    filter(VERSION == rds$RDS_VERRDS & ANNEE == rds$RDS_ANNEESOR)
  
  champs_info_sup <- c("RDS_GHS9610", "RDS_GHS9611", "RDS_GHS9612", "RDS_GHS9619", "RDS_GHS9620", "RDS_GHS6523", "RDS_GHS9621", "RDS_NBAPHERESE",
              "RDS_GHS9622", "RDS_GHS9623", "RDS_GHS9625", "RDS_GHS9631", "RDS_GHS9632", "RDS_GHS9633", "RDS_TYPEPO",
              "RDS_NBHEMODIAL", "RDS_NBENTDIALAUTO", "RDS_NBENTDIALCONT", "RDS_NBENTHEMODIAL", "RDS_NBCAISSON", "RDS_NBRXTHPED",
              "RDS_AVASTIN", "RDS_ALIMTA")
  
  info <- rds %>% gather(key = "champ") %>% filter(champ %in% champs_info_sup) %>% 
    left_join(dico, by = c("champ" = "NOM_CHAMP")) %>% 
    select(champ, lib = LIBELLE_CHAMP, value) %>% 
    as.data.frame() %>% 
    filter(!is.na(value) & !(value %in% c("", "0", "00", "000")))

  return(info)
}

