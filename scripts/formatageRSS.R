



# Fonction support ----

#' convertit une date en chaine de caractères de type ddmmyyyy
#' @param date une date 
convertDate <- function(date){
  paste0(format(date, '%d'), format(date, '%m'), format(date, '%Y'))
}



#
# Fonction de formatage v18 des inputs pour écriture dans le fichier inputRSS.txt ----
#

format_RSS_018 <-  function(annee, admin, UM, DP, DR, seances, DAS, CCAM, 
                       RUM_number, duree_totale_sejour, nb_nuits_avant_entree_UM){
  
  
  # Définition et formattage des dates d'entrée/sortie dans l'UM ----
  
  date_fin_sejour <- if (annee == 2018) as.Date("2018-03-10")  # définition en dur d'une date de de fin de séjour postérieure au 1er mars 2018
  else if (annee == 2017) as.Date("2017-03-10")
  date_debut_sejour <- date_fin_sejour - duree_totale_sejour
  date_entree <- date_debut_sejour + nb_nuits_avant_entree_UM  # date d'entrée dans l'UM
  date_sortie <- date_entree + UM$nb_nuits()  # date de sortie de l'UM
  
  # 
  # Bloc de taille fixe : 177 caractères obligatoires ----
  #   paramètres définis dans l'ordre de présentation du format de RSS
  #   
  
  # Caractères invariants quel que soit le RSS
  numero_finess <- "123456789" # obligatoire, 9 caractères
  version_format_rum <- "018" # obligatoire, 3 caractères
  numero_RSS <- "12345678901234567890" # obligatoire, 20 caractères
  numero_admin_local_sejour <- "12345678901234567890" # obligatoire, 20 caractères
  
  
  # numero du RUM, obligatoire, 10 caractères
  numero_RUM <- str_pad(string = RUM_number, width = 10, side = "right")
  
  
  # DDN calculé en fonction de l'âge et de la date d'entrée, obligatoire, 8 caractères
  date_naissance <- if (admin$type_sejour() == 2) {  # 2 = néonatalogie
    convertDate(date_debut_sejour - admin$age_jours())
  } else {
    convertDate(date_debut_sejour - admin$age_annees() * 365)
  }
  
  
  sexe <- admin$sexe() # obligatoire, 1 caractère
  
  
  # Caractères invariants quel que soit le RSS
  numero_unite_med <- str_pad(string = RUM_number, width = 4, side = "right") # obligatoire, 4 caractères, doit matcher le numero de RUM pour la cohérence avec l'autorisation d'ium
  type_autorisation_lit <- "  " # facultatif, 2 caractères (renvoie un erreur si mis à 00)
  
  
  date_entree_um <- convertDate(date_entree) # obligatoire, 8 caractères
  mode_entree_um <- UM$mode_entree() # obligatoire, 1 caractère
  provenance <- UM$code_provenance()
  date_sortie_um <- convertDate(date_sortie) # obligatoire, 8 caractères
  mode_sortie_um <- UM$mode_sortie() # obligatoire, 1 caractère
  destination <- UM$code_destination() #1 caractère, cas 9-décès déjà prévu par UM
  
  
  # Caractère invariant
  code_postal_residence <- "75001" # obligatoire, 5 caractères
  
  
  poids_nne <- if (admin$type_sejour() != 2) {  # facultatif, 4 caractères
    "0000" 
  } else { 
    str_pad(UM$poids_NN(), 4, side = "left", pad = 0)
  }
  
  age_gestationnel <- if (admin$type_sejour() == 1) {  # facultatif, 2 caractères
    "00" 
  } else {
    str_pad(admin$AG(), 2, side = "left", pad = 0)
  }
  
  ddr <- if (admin$type_sejour() == 3) {
    convertDate(date_debut_sejour - admin$AG() * 7)
  } else strrep(x = " ", times = 8) # facultatif, 8 caractères
  
  
  # Nombre de séances, facultatif, 2 caractères
  nb_seances <- str_pad(seances$nb_seances(), 2, side = "left", pad = 0)

  
  # Nombre de diagnostics associés significatifs, obligatoire, 2 caractères
  nDA <- str_pad(DAS$nb_DAS_actifs, 2, side = "left", pad = 0)

  
  # Nombre de DAD, fixé de façon invariante à 0, obligatoire, 2 caractères
  nDAD <- "00"
  
  
  # Nombre d'actes CCAM, obligatoire, 3 caractères
  #   !! attention, on génère autant d'actes que de dates de réalisations et de codes activité différents !!
  nbTotalActes <- 0
  if (CCAM$nb_CCAM_actifs > 0) {  # indispensable pour éviter de générer une séquence 1:0
    for (i in 1:CCAM$nb_CCAM_actifs) {
      for (j in 1:length(CCAM$codes_activite_CCAM_actifs[[paste0("CCAM", i)]]())) {
        for (k in 1:length(CCAM$dates_CCAM_actifs[[paste0("CCAM", i)]]())) {
          nbTotalActes <- nbTotalActes + 1
        }
      }
    }
  }
  nZA <- str_pad(nbTotalActes, 3, side = "left", pad = 0)

  
  # code DP obligatoire, 8 caractères
  DP <- str_pad(DP$code_DP(), 8, side = "right")
  
  
  # code DR obligatoire, 8 caractères
  DR <- str_pad(DR$code_DR(), 8, side = "right")
  
  
  # score IGS-2, facultatif, 3 caractères
  IGS2 <- str_pad(UM$score_igs2(), 3, side = "left", pad = 0)
  
  
  # Bloc de données supplémenatires facultatives sur 33 caractères 
  #   (attention le 1er pour confirmation du codage ne doit pas être saisi par excès sinon une erreur bloquante est renvoyée par la FG)
  donnees_sup <- strrep(x = " ", times = 33)
  donnees_sup_2 <- paste0("1", strrep(x = " ", times = 32)) # cas où la confirmation du codage est nécessaire
  
  
  
  #
  # Bloc de taille variable : zones DAS ----
  #  
  DAS <- if (DAS$nb_DAS_actifs > 0){ # required to avoid genereating a 1:0 sequence when no DAS
    s <- ""
    for (i in 1:DAS$nb_DAS_actifs) {
      s <- paste0(s, DAS$codes_DAS_actifs[[paste0("DAS", i)]]())  # codes_DAS_actifs déjà mis au format 8 caractères
    }
    s
  } else "" # empty string when there is no DAS
  
  
  
  #
  # Bloc de taille variable : zones actes CCAM ----
  #   un même acte est saisi autant de fois qu'il a de codes activité différents et de dates de réalisations différentes
  #    
  CCAM <- if (CCAM$nb_CCAM_actifs > 0) { # required to avoid generating a 1:0 sequence when no CCAM
    s <- ""
    for (i in 1:CCAM$nb_CCAM_actifs) {
      for (k in 1:length(CCAM$dates_CCAM_actifs[[paste0("CCAM", i)]]())) {
        for (j in 1:length(CCAM$codes_activite_CCAM_actifs[[paste0("CCAM", i)]]())){
          s <- paste0(
            s, 
            convertDate(date_entree + CCAM$dates_CCAM_actifs[[paste0("CCAM", i)]]()[k]), # date ddmmyyyy
            CCAM$param_CCAM_actifs[[paste0("CCAM", i)]](),  # pre-formattage sur 11 caracatères déjà fait déjà fait
            CCAM$codes_activite_CCAM_actifs[[paste0("CCAM", i)]]()[j], # code activité
            strrep(x = " ", times = 7), # 7 caractères correspondant à des codes facultatifs non utiles
            "01" # 1 seule réalisation possible de l'acte CCAM pour une date donnée et un code activité donné
          )
        }
      }
    }
    s
  } else "" # empty string in case there is no acte CCAM
  
  
  
  # 
  # Concaténation des variables et return ----
  # 
  rum_partie_invariante <- paste0(
    numero_finess, version_format_rum, numero_RSS, numero_admin_local_sejour, numero_RUM, 
    date_naissance,
    sexe, 
    numero_unite_med, type_autorisation_lit, 
    date_entree_um, mode_entree_um, provenance, 
    date_sortie_um, mode_sortie_um, destination, 
    code_postal_residence,
    poids_nne, age_gestationnel, ddr, 
    nb_seances, 
    nDA, nDAD, nZA, 
    DP, DR, 
    IGS2
  )
  
  rum_1 <- paste0(rum_partie_invariante, donnees_sup, DAS, CCAM)
  rum_2 <- paste0(rum_partie_invariante, donnees_sup_2, DAS, CCAM)
  
  return(list(no_confirm_codage = rum_1, with_confirm_codage = rum_2))
  
}




