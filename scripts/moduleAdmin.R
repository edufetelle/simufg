
############################################################################################################### 
# 
# 
#         Module de saisie des infos administratives du patient
# 
#         Renvoie en sortie une liste de valeurs réactives (éléments à invoquer avec () ) :
#           - type de séjour : type_sejour (1 - classique, 2 - neonat, 3 - obstétrique)
#           - age en années : age_annees (égal à 0 si neonat)
#           - age en jours : age_jours (non nul si néonat, 0 sinon)
#           - sexe
#           - age gestationnel : AG (champ vide si séjour classique)
#           - no_admin_error : boolean indiquant s'il existe une erreur de saisie sur l'âge ou l'AG
#           (i.e. champ effacé et laissé vide par erreur)
# 
# 
###############################################################################################################



library(shiny)


#
# UI ----
# 
moduleAdmin_UI <- function(id){
  ns <- NS(id)
  
  fluidRow(
    column(
      width = 3, 
      # Choix du type de séjour
      wellPanel(
        radioButtons(
          inputId = ns("type_sejour"), label = h3("Type de séjour"), inline = F,
          choices = c("Classique" = 1, "Néonatalogie" = 2, "Obstétrique" = 3) 
        )
      )),
    column(
      width = 9,
      # Infos administratives patient obligatoires ----
      uiOutput(outputId = ns("infos_admin")))
  )
  
}


# 
# server ----
# 
moduleAdmin <- function(input, output, session){
  ns <- session$ns
  
  # Gestion de l'UI des infos administratives selon le type de séjour ----
  output$infos_admin <- renderUI(
    
    wellPanel(
      fluidRow(h3("Informations patient obligatoires")),
      fluidRow(
        column(width = 3, uiOutput(outputId = ns("age_picker"))),
        if (input$type_sejour == 1){
          column(
            width = 2, 
            radioButtons(
              inputId = ns("sexe"), 
              label = "Sexe", 
              choices = list("M" = 1, "F" = 2), inline = T)
          )
        } else if (input$type_sejour == 2){
          list(column(
            width = 2, 
            radioButtons(
              inputId = ns("sexe"), 
              label = "Sexe", 
              choices = list("M" = 1, "F" = 2), inline = T)
          ),
          column(
            width = 4,
            numericInput(inputId = ns("age_gesta"), label = "AG à la naissance (en SA)", value = 38, min = 0, max = 45, step = 1)
          ))
        } else {
          column(
            width = 4,
            numericInput(inputId = ns("age_gesta"), label = "AG à l'admission (en SA)", value = 38, min = 0, max = 45, step = 1)
          )
        }
      ),
      fluidRow(column(width = 9, htmlOutput(outputId = ns("error_message"))))
    )
    
  )
  
  
  # Age picker ----
  output$age_picker <- renderUI(
    if (input$type_sejour == 2){
      numericInput(inputId = ns("age"), label = "Age à l'admission (en jours)", value = 0, min = 0, step = 1)
    } else {
      numericInput(
        inputId = ns("age"), label = "Age en années", 
        value = ifelse(test = input$type_sejour == 3, yes = 30, no = 60), min = 0, step = 1)
    }
  )
  
  
  # Error mesages ----
  output$error_message <- renderText({
    if (!is.null(input$age) && is.na(input$age)) {
      '<font color="red" size=3><b><i>Erreur : saisir une valeur pour l\'âge du patient !</b></i></font>'
    } else if (input$type_sejour != 1 && !is.null(input$age_gesta) && is.na(input$age_gesta)) {
      '<font color="red" size=3><b><i>Erreur : saisir une valeur pour l\'âge gestationnel !</b></i></font>'
    } else if (input$type_sejour != 1 && !is.null(input$age_gesta) && input$age_gesta > 45) {
      '<font color="red" size=3><b><i>Erreur : âge gestationnel impossible !</b></i></font>'
    }
  })
  
  
  
  # return values ----
  return(list(
    type_sejour = reactive(input$type_sejour), 
    age_annees = reactive({
      if(input$type_sejour !=2) input$age else 0
    }), 
    age_jours = reactive({
      if(input$type_sejour == 2) input$age else 0
    }),
    sexe = reactive({
      if (input$type_sejour == 3) 2 else input$sexe
    }),
    AG = reactive({
      if (input$type_sejour == 1) "" else input$age_gesta
    }),
    no_admin_error = reactive({
      (!is.null(input$age) && !is.na(input$age)) &&
        (input$type_sejour == 1 || !is.null(input$age_gesta) && !is.na(input$age_gesta) && input$age_gesta <= 45)
    })
  ))
  
  
}




#===============================================================================#
#
# Test du module ----
# 

# library(shinythemes)
# 
# ui <- fluidPage(
#   theme = shinytheme("cerulean"),
#   moduleAdmin_UI("admin"),
#   textOutput("sortie")
# )
# 
# 
# server <- function(input, output, session){
#   admin <- callModule(moduleAdmin,"admin")
#   output$sortie <- renderText(paste0(
#     "Type de séjour : ", admin$type_sejour(),
#     " | Age (années) : ", admin$age_annees(),
#     " | Age (jours) : ", admin$age_jours(),
#     " | sexe : ", admin$sexe(),
#     " | AG : ", admin$AG(),
#     " | no_admin_error : ", admin$no_admin_error()
#   ))
# }
# 
# 
# shinyApp(ui = ui, server = server)

