
############################################################################################
#
#
#   Module de Gestion des infos relatives à l'UM
#   
#   Nécessite comme arguments :
#     - l'age en années à passer en reactive
#     - le type de séjour à passer en reactive
#     - la table des codes d'autorisation d'UM ium_table
#   
#   Renvoie en sortie une liste de valeurs reactives comprenant :
#     - le code correspondant au type d'UM sélectionné : code_ium
#     - le score IGS2 : score_igs2
#     - le code mode d'entrée dans l'unité : mode_entree
#     - le code provenance : code_provenance
#     - le code mode de sortie de l'unité : mode_sortie
#     - le code destination : code_destination
#     - le nombre de nuits passées dans l'unité : nb_nuits
#     - en cas de séjour de néonat, le poids d'entrée du NN : poids_NN
#     - un boolean signalant une erreur de saisie empechant l'execution de la FG : no_UM_error
#     
#
#   Affiche des messages d'erreurs en cas de champs vides pour la durée de séjour et le score IGS
#
#
############################################################################################






# 
# UI ----
# 
moduleUM_UI <- function(id){
  ns <- NS(id)
  
  tagList(
    
    # Type UM +/- score IGS-2
    fluidRow(column(width = 12, uiOutput(outputId = ns("type_um")))),
    fluidRow(column(width = 12, uiOutput(outputId = ns("igs2")))),
    
    
    # Détails entrée/sortie dans l'unité ----
    wellPanel(
      fluidRow(
        column(width = 4, h4("Entrée dans l'unité")),
        column(width = 4, h4("Sortie de l'unité"))
      ),
      fluidRow(
        column(
          width = 4,
          selectInput(
            inputId = ns("mode_entree"), width = '75%',
            label = "Mode d'entrée",
            choices = list("6 - Mutation" = "6", "7 - Transfert" = "7", "8 - Domicile" = "8", "0 - Prestation inter-étbl" = "0"),
            selected = "8"
          )
        ),
        column(
          width = 4,
          selectInput(
            inputId = ns("mode_sortie"), width = '75%',
            label = "Mode de sortie",
            choices = list("6 - Mutation" = "6", "7 - Transfert" = "7",
                           "8 - Domicile" = "8", "9 - Décès" = "9", "0 - Prestation inter-étbl" = "0"),
            selected = "8")
        ),
        column(
          width = 4,
          # Nombre de nuits dans l'unité
          numericInput(inputId = ns("nb_nuits"), label = "Nombre de nuits", value = 3, min = 0, step = 1, width = '75%')
        )
      ),
      fluidRow(
        # Provenance dépendante du mode d'entrée
        column(
          width = 4,
          uiOutput(outputId = ns("input_provenance"))
        ),
        # Destination dépendante du mode de sortie
        column(
          width = 4,
          conditionalPanel(
            condition = paste0("input['", ns("mode_sortie"), "'] != 9"), # pas de destination nécessaire en cas de décès
            uiOutput(outputId = ns("input_destination"))
          )
        ),
        column(width = 4, htmlOutput(ns("error_nb_nuits"))) # optional error message in case nb_nuits erased by mistake
      ),
      fluidRow(
        # Poids du nouveau-né à l'entrée dans l'UM en cas de séjour de néonat
        column(
          width = 4,
          uiOutput(outputId = ns("inputPoidsNN"))
        ),
        column(width = 8, htmlOutput(ns("error_poids_nn")))  # optional error message in case poids NN erased by mistake or >9999g
      )
    )
    
  )
  
}



# 
# server ----
# 
moduleUM <- function(input, output, session, type_sejour = reactive(1), age = reactive(60), ium_table){
  ns <- session$ns
  
  # 
  # Gestion du choix du type d'UM (à but de générer les autorisations adéquates pour le calcul des forfaits) ----
  # 
  output$type_um <- renderUI(
    if (type_sejour() == 2){
      selectInput(
        inputId = ns("code_ium"), label = "Type d'UM", 
        choices = filter(ium_table, code_um %in% c("04", "05", "06"))$lib
      )
    } else if (type_sejour() == 3){
      selectInput(
        inputId = ns("code_ium"), label = "Type d'UM", 
        choices = filter(ium_table, !(code_um %in% c("04", "05", "06")))$lib,
        selected = filter(ium_table, code_um == "71")$lib
      )
    } else {
      selectInput(
        inputId = ns("code_ium"), label = "Type d'UM", 
        choices = filter(ium_table, !(code_um %in% c("04", "05", "06")))$lib,
        selected = filter(ium_table, code_um == "29")$lib
      )
    }
  )
  
  
  
  ##
  ## Remarques générales sur l'initialisation des input$... : ----
  ##    le code qui suit présente quelques "lourdeurs" avec la création d'un boolean IsIniatialized() et
  ##    l'utilisation de conditions (!is.null(input$... ) && input$...)
  ##    La raison est la suivante : l'affichage et la gestion des erreurs liées à input$score_igs2 et input$poids_nn
  ##    dépendent de ces valeurs d'inputs. Or ces inputs sont créées par une fonction renderUI soumise à conditions
  ##    (elles n'existent pas au lancement). Lorsque ces inputs sont créées (quand les conditions sont satisfaites)
  ##    il y a un (très) court laps de temps pendant lequel elle sont encore de type NULL, ce qui renvoie un message d'erreur
  ##    pour l'exécution des fonction dont elles dépendent. L'utilisation d'un test préalable !is.null(input$...) permet de 
  ##    pallier ce problème.
  ##    
  
  
  # 
  # Input du score IGS-2 si réanimation ou USI ----
  # 
  code_iumIsInitialized <- reactiveVal(F)
  observeEvent(eventExpr = input$code_ium, handlerExpr = code_iumIsInitialized(T))
  
  igs2IsHidden <- reactive(
    if(substr(input$code_ium, start = 1 , stop = 2) %in% c("01", "02", "03", "13", "14", "18")) F
    else T
  )
  output$igs2 <- renderUI(
    if(code_iumIsInitialized() && !igs2IsHidden()){
      fluidRow(
        column(
          width = 3,
          numericInput(inputId = ns("score_igs2"), label = "Score IGS-2", value = 0, min = 0, max = 164, step = 1, width = '150')
        ),
        column(width = 9, br(), h5(htmlOutput(outputId = ns("score_igs2_sansEffetAge"))))
      )
    }
  )
  output$score_igs2_sansEffetAge <- renderText({
    if (!is.null(input$score_igs2) && is.na(input$score_igs2)) {
      '<font color="red"><b><i>Erreur : saisir une valeur pour le score IGS-2 !</b></i></font>'
    } else if(input$score_igs2 > 164) {
      '<font color="red"><b><i>Erreur : valeur du score IGS-2 non valide !</b></i></font>'
    } else if (!is.na(age())) {
      paste0("Score IGS-2 sans effet de l'âge : ",
             if (age() < 40) input$score_igs2
             else if (age() < 60) max(input$score_igs2 - 7, 0)
             else if (age() < 70) max(input$score_igs2 - 12, 0)
             else if (age() < 75) max(input$score_igs2 - 15, 0)
             else if (age() < 80) max(input$score_igs2 - 16, 0)
             else max(input$score_igs2 - 18, 0),
             " pts")
    } else { '<font color="red"><b><i>Erreur : saisir une valeur pour l\'âge du patient !</b></i></font>'}
  })
  
  
  
  #
  # Provenance et Destination ----
  #
  output$input_provenance <- renderUI(
    if(input$mode_entree == 6){
      selectInput(
        inputId = ns("provenance"), label = "Origine", width = '75%',
        choices = c("1 - MCO" = "1", "2 - SSR" = "2", "3 - USLD" = "3", "4 - PSY" = "4", "6 - HAD" = "6"),
        selected = "1")
    } else if (input$mode_entree == 7) {
      selectInput(
        inputId = ns("provenance"), label = "Origine", width = '75%',
        choices = c("1 - MCO" = "1", "2 - SSR" = "2", "3 - USLD" = "3", "4 - PSY" = "4", "6 - HAD" = "6", "R - Réanimation" = "R"),
        selected = "1")
    } else if (input$mode_entree == 0) {
      selectInput(
        inputId = ns("provenance"), label = "Origine", width = '75%',
        choices = c("1 - MCO" = "1", "2 - SSR" = "2", "3 - USLD" = "3", "4 - PSY" = "4", "R - Réanimation" = "R"),
        selected = "1")
    } else { # mode entree == 8
      selectInput(
        inputId = ns("provenance"), label = "Origine", width = '75%',
        choices = c("-" = " ", "5 - Urgences" = "5"),
        selected = "1")
    }
  )
  
  output$input_destination <- renderUI(
    if(input$mode_sortie == 6){
      selectInput(
        inputId = ns("destination"), label = "Destination", width = '75%',
        choices = c("1 - MCO" = "1", "2 - SSR" = "2", "3 - USLD" = "3", "4 - PSY" = "4", "6 - HAD" = "6"),
        selected = "1")
    } else if (input$mode_sortie == 7) {
      selectInput(
        inputId = ns("destination"), label = "Destination", width = '75%',
        choices = c("1 - MCO" = "1", "2 - SSR" = "2", "3 - USLD" = "3", "4 - PSY" = "4", "6 - HAD" = "6"),
        selected = "1")
    } else if (input$mode_sortie == 0) {
      selectInput(
        inputId = ns("destination"), label = "Destination", width = '75%',
        choices = c("1 - MCO" = "1", "2 - SSR" = "2", "3 - USLD" = "3", "4 - PSY" = "4"),
        selected = "1")
    } else { # mode de sortie == 8
      selectInput(
        inputId = ns("destination"), label = "Destination", width = '75%',
        choices = c("-" = " ", "7 - Médico-social" = "7"),
        selected = "1")
    }
  )
  
  
  
  # 
  # Message d'erreur en cas de durée de séjour effacée et champ laissé vide ----
  # 
  output$error_nb_nuits <- renderText({
    if (is.na(input$nb_nuits)) {
      '<font color="red"><b><i>Erreur : saisir une valeur pour la durée du séjour !</b></i></font>'
    }
  })
  
  
  #
  # Poids du NN en cas de séjour de néonat ----
  #
  output$inputPoidsNN <-  renderUI(
    if (type_sejour() == 2) {
      numericInput(inputId = ns("poids_nn"), label = "Poids d'entrée en g", value = 3500, min = 400, max = 9999, step = 1, width = '75%')
    }
  )
  
  
  # 
  # Message d'erreur en cas de poids du NN effacé et champ laissé vide ou poids trop élevé ----
  # 
  output$error_poids_nn <- renderText({
    if (type_sejour() == 2) {
      if (!is.null(input$poids_nn) && is.na(input$poids_nn)) {
        '<font color="red"><b><i><br><br>Erreur : saisir une valeur pour le poids d\'entrée du nouveau-né !</b></i></font>'
      } else if (!is.null(input$poids_nn) && input$poids_nn > 9999) {
        '<font color="red"><b><i><br><br>Erreur : poids d\'entrée du nouveau-né impossible !</b></i></font>'
      }
    }
  })
  
  
  
  
  # 
  # Return values ----
  # 
  return(list(
    code_ium = reactive(substr(input$code_ium, 1, 3)), # code UM sur 3 caractères
    score_igs2 = reactive({
      if (code_iumIsInitialized() && !igs2IsHidden()) input$score_igs2 else 0  # initialisation préalable nécessaire pour éviter des erreurs d'évaluation
    }),
    mode_entree = reactive(input$mode_entree),
    code_provenance = reactive(input$provenance),
    mode_sortie = reactive(input$mode_sortie),
    code_destination = reactive({
      if (input$mode_sortie == 9) " " else input$destination
    }),
    nb_nuits = reactive(input$nb_nuits),
    poids_NN = reactive({
      if (type_sejour() == 2) input$poids_nn else 0
    }),
    no_UM_error = reactive(
      !is.null(input$nb_nuits) && !is.na(input$nb_nuits) &&
        (!code_iumIsInitialized() || igs2IsHidden() || (!is.null(input$score_igs2) && !is.na(input$score_igs2) && input$score_igs2 <= 164)) &&
        (type_sejour() != 2 || !is.null(input$poids_nn) && !is.na(input$poids_nn) && input$poids_nn <= 9999)
    )
  ))
  
  
  
}



############################################
#
# Test du module ----
# 

# library(feather)
# library(shinythemes)
# library(shiny)
# library(dplyr)
# 
# path <- "C:/Users/E.DUFETELLE/Documents/Projets_R/shinyApps/newSimuFG/newSimuFGv3"
# ium_table <- read_feather(paste0(path,"/tables_libelles/ium.feather"))
# 
# 
# ui <- fluidPage(
#   theme = shinytheme("cerulean"),
#   moduleUM_UI("UM"),
#   textOutput("sorties")
# )
# 
# 
# server <- function(input, output, session){
#   UM_data <- callModule(moduleUM, "UM", ium_table = ium_table, type_sejour = reactive(2))
#   output$sorties <- renderText(
#     paste0(
#       "code IUM : ", UM_data$code_ium(), "(", nchar(UM_data$code_ium()), ")",
#       " | score IGS-2 : ", UM_data$score_igs2(),
#       " | mode entrée : ", UM_data$mode_entree(),
#       " | provenance : ", UM_data$code_provenance(), "(", nchar(UM_data$code_provenance()), ")",
#       " | mode sortie : ", UM_data$mode_sortie(),
#       " | destination : ", UM_data$code_destination(), "(", nchar(UM_data$code_destination()), ")",
#       " | Nb de nuits : ", UM_data$nb_nuits(),
#       " | Poids du NN : ", UM_data$poids_NN(),
#       " | no_UM_error : ", UM_data$no_UM_error()
#     )
#   )
# }
# 
# 
# shinyApp(ui = ui, server = server)



