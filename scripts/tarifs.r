# 
# script de lecture et de mise en forme 
# des tables des tarifs et de suppléments tarifaires
# (d'après la fonction regroupeur de Stéphane Said)
# 


# library(readr)
# library(purrr)
# library(magrittr)


fic_tarif_pri <-list.files(path = "./tarifs", pattern = "ghs_pri\\.csv", 
                           recursive = TRUE, full.names = TRUE)

tarifs <- map_df(fic_tarif_pri, read_csv2, col_types = cols(.default = "c"))
                 # locale = locale(encoding = "latin9"))

names(tarifs) <- c("NUMGHS", "CMD", "CKMZ", "GHM", "LIBELLE", "SEUIL_BAS",
                   "SEUIL_HAUT", "TARIF_GHS", "FORFAIT_EXB", "TARIF_EXB",
                   "TARIF_EXH", "DATE_EFFET")

tarifs <- tarifs %>% 
  mutate_at(vars(SEUIL_BAS:TARIF_EXH), funs(str_replace_all(., ",","."))) %>% 
  mutate_at(vars(SEUIL_BAS:TARIF_EXH), as.numeric) %>% 
  mutate(NUMGHS = str_pad(NUMGHS, width = 4, pad = "0"),
         DATE_EFFET = as.Date(DATE_EFFET, "%d/%m/%Y"),
         FG_VERSION = as.integer(format(DATE_EFFET, "%Y")))


fic_supplements_pri <- list.files(path = "./tarifs", pattern = "sup_pri\\.csv",
                                  recursive = TRUE, full.names = TRUE)

supplements <- map_df(fic_supplements_pri, ~{
  annee <- str_extract(.x, "20[0-9]{2}")
  output <- read_csv2(.x, col_types = cols(.default = "c"))
  output$FG_VERSION <- as.integer(annee)
  
  return(output)
})


supplements <- supplements %>% 
  mutate(TARIF = str_replace_all(TARIF, ",", "."),
         TARIF = as.numeric(TARIF)) %>% 
  spread(key = CODE, value = TARIF, fill = 0)

