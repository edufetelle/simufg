
library(shiny)

fluidPage(
  
  # themeSelector(),
  theme = shinytheme("cerulean"),
  useShinyjs(),
  
  
  # Application title ----
  titlePanel(title = "Simulateur de la fonction groupage"),
  hr(),
  
  
  # Modules UI ----
  moduleAdmin_UI("admin"),
  # 1er RUM plus besoin d'être présent de façon obligatoire : permet à l'utilisateur de l'effacer/remplacer si besoin

  
  # Add RUM button ----
  div(id = "addRUMButton",
      fluidRow(
        column(width = 3, actionButton(inputId = "addRUM", label = tags$b("Ajouter un RUM"), class = "btn btn-info")),
        column(width = 9, htmlOutput(outputId = "numberOfRUM"))
      )
  ),
  
  
  # Bouton groupage ----
  hr(),
  fluidRow(
    column(width = 12, selectInput(inputId = "versionFG", label = "Version de la fonction groupage", choices = c("2017", "2018"), selected = "2018"))
  ),
  fluidRow(
    column(width = 2, actionButton(inputId = "group", label = tags$b("Grouper"), class = "btn btn-success")),
    column(width = 10, htmlOutput(outputId = "erreurAvantGroupage"))
  ),
  hr(),
  
  
  # Bouton reset ----
  actionButton(inputId = "reset", label = tags$b("Reset all"), class = "btn btn-danger"),
  div(id = "endOfPage", hr())
  
  
  # Download button ----
  # downloadButton("download", label = "download")
  # actionButton("click", "click")
  
  # Sortie fonction groupage ----
  # moduleDisplayResults_UI("resultsFG")
  
)

